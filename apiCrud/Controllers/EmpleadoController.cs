﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using apiCrud.DataEntity;
using HttpGetAttribute = System.Web.Mvc.HttpGetAttribute;
using HttpPostAttribute = System.Web.Mvc.HttpPostAttribute;

namespace apiCrud.Controllers
{
    public class EmpleadoController : ApiController
    {
        private TESTEntities db = new TESTEntities();

        // GET: Empleado
        [System.Web.Http.HttpGet]
        [System.Web.Mvc.Route("api/empleado/")]
        public IHttpActionResult Index()
        {
            IList<Empleado> data = null;
            data = db.Empleado.ToList<Empleado>();
            return Ok(data);
        }



        // GET: Tipo_usuario/Details/5
        [System.Web.Http.HttpGet]
        [System.Web.Mvc.Route("api/empleado/{id}")]
        public IHttpActionResult Details(int? id)
        {
            if (id == null)
            {
                return BadRequest("Dato requerido");
            }
            Empleado data = db.Empleado.Find(id);
            if (data == null)
            {
                return NotFound();
            }
            return Ok(data);
        }


        [System.Web.Http.HttpPost]
        [System.Web.Mvc.Route("api/empleado/")]
        public IHttpActionResult Create(Empleado empleado)
        {
            try
            {
                db.Empleado.Add(new Empleado()
                {
                    id = empleado.id,
                    nombre = empleado.nombre,
                    apellido = empleado.apellido,
                    departamento = empleado.departamento,
                    puesto = empleado.puesto,
                    salario = empleado.salario
                });
                db.SaveChanges();
                return Ok("OK");
            }
            catch (Exception ex)
            {
                return BadRequest("Error en Creacion: " + ex.Message);
            }
        }

        [System.Web.Http.HttpPut]
        [System.Web.Mvc.Route("api/empleado/{id}")]
        public IHttpActionResult Edit(int? id, Empleado empleado)
        {

            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                db.Entry(empleado).State = EntityState.Modified;
                db.SaveChanges();
                return Ok("Dato modificado exitosamente");
            }
            catch (Exception ex)
            {
                return BadRequest("Error en modificacion: " + ex.Message);
            }
        }

        [System.Web.Http.HttpDelete]
        [System.Web.Mvc.Route("api/empleado/{id}")]
        public IHttpActionResult Delete(int id)
        {
            try
            {
                Empleado empleado = db.Empleado.Find(id);
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                db.Entry(empleado).State = EntityState.Deleted;
                db.SaveChanges();
                return Ok("Datos eliminados exitosamente");
            }
            catch (Exception ex)
            {
                return BadRequest("Error en Eliminación: " + ex.Message);
            }

        }
    }
}