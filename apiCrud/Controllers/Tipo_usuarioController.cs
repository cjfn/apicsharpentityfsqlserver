﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using apiCrud.DataEntity;
using AuthorizeAttribute = System.Web.Http.AuthorizeAttribute;
using HttpGetAttribute = System.Web.Http.HttpGetAttribute;
using RouteAttribute = System.Web.Http.RouteAttribute;
using RoutePrefixAttribute = System.Web.Http.RoutePrefixAttribute;

namespace apiCrud.Controllers
{


    [Authorize]
    [RoutePrefix("api/tipo_usuario")]
    public class Tipo_usuarioController : ApiController
    {
        private TESTEntities db = new TESTEntities();

        // GET: Tipo_usuario
        // GET: Clientes
        [HttpGet]
        [Route("api/tipo_usuario")]
        //[System.Web.Mvc.RoutePrefix("api/tipo_usuario/")]

        public IHttpActionResult Index()
        {
            IList<view_tipo_usuario> data = null;
            data = db.view_tipo_usuario.ToList<view_tipo_usuario>();
            return Ok(data);
        }



        // GET: Tipo_usuario/Details/5
        [System.Web.Http.HttpGet]
        [System.Web.Mvc.Route("api/tipo_usuario/{id}")]
        public IHttpActionResult Details(int? id)
        {
            if (id == null)
            {
                return BadRequest("Dato requerido");
            }
            view_tipo_usuario data = db.view_tipo_usuario.Find(id);
            if (data == null)
            {
                return NotFound();
            }
            return Ok(data);
        }


        [System.Web.Http.HttpPost]
        [System.Web.Mvc.Route("api/tipo_usuario/")]
        public IHttpActionResult Create(Tipo_usuario tipo_usuario)
        {
            try
            {
                db.Tipo_usuario.Add(new Tipo_usuario()
                {
                    id = tipo_usuario.id,
                    nombre = tipo_usuario.nombre,
                    descripcion = tipo_usuario.descripcion,
                    created_at = DateTime.Now,
                    updated_at = null


                });
                db.SaveChanges();
                return Ok("OK");
            }
            catch (Exception ex)
            {
                return BadRequest("Error en Creacion: " + ex.Message);
            }
        }

        [System.Web.Http.HttpPut]
        [System.Web.Mvc.Route("api/tipo_usuario/{id}")]
        public IHttpActionResult Edit(int? id, Tipo_usuario tipo_usuario)
        {
            Tipo_usuario t_usuario = db.Tipo_usuario.Find(id);
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                t_usuario.id = tipo_usuario.id;
                t_usuario.nombre = tipo_usuario.nombre;
                t_usuario.descripcion = tipo_usuario.descripcion;
                t_usuario.created_at = t_usuario.created_at;
                t_usuario.updated_at = DateTime.Now;
                db.Entry(t_usuario).State = EntityState.Modified;
                db.SaveChanges();
                return Ok("Dato modificado exitosamente");
            }
            catch (Exception ex)
            {
                return BadRequest("Error en modificacion: " + ex.Message);
            }
        }

        [System.Web.Http.HttpDelete]
        [System.Web.Mvc.Route("api/tipo_usuario/{id}")]
        public IHttpActionResult Delete(int id)
        {
            try
            {
                
                Tipo_usuario tipo_usuario = db.Tipo_usuario.Find(id);
                var entry = db.Entry(tipo_usuario);
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                if (entry.State == EntityState.Detached)
                {
                    //Attached it since the record is already being tracked
                    db.Tipo_usuario.Attach(tipo_usuario);
                }
                //db.Entry(tipo_usuario).State = EntityState.Deleted;
                db.Tipo_usuario.Remove(tipo_usuario);
                db.SaveChanges();
                return Ok("Datos eliminados exitosamente");
            }
            catch (Exception ex)
            {
                return BadRequest("Error en Eliminación: " + ex.Message);
            }

        }
    }
}
