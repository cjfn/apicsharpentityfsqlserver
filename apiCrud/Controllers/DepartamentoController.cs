﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using apiCrud.DataEntity;
using HttpGetAttribute = System.Web.Mvc.HttpGetAttribute;
using HttpPostAttribute = System.Web.Mvc.HttpPostAttribute;


namespace apiCrud.Controllers
{
    public class DepartamentoController : ApiController
    {
        private TESTEntities db = new TESTEntities();

        // GET: Departamento
        // GET: Clientes
        [System.Web.Http.HttpGet]
        [System.Web.Mvc.Route("api/departamento/")]
        public IHttpActionResult Index()
        {
            IList<Departamento> data = null;
            data = db.Departamento.ToList<Departamento>();
            return Ok(data);
        }



        // GET: Departamento/Details/5
        [System.Web.Http.HttpGet]
        [System.Web.Mvc.Route("api/departamento/{id}")]
        public IHttpActionResult Details(int? id)
        {
            if (id == null)
            {
                return BadRequest("Dato requerido");
            }
            Departamento data = db.Departamento.Find(id);
            if (data == null)
            {
                return NotFound();
            }
            return Ok(data);
        }


        [System.Web.Http.HttpPost]
        [System.Web.Mvc.Route("api/departamento/")]
        public IHttpActionResult Create(Departamento departamento)
        {
            try
            {
                db.Departamento.Add(new Departamento()
                {
                    id = departamento.id,
                    nombre = departamento.nombre,
                  

                });
                db.SaveChanges();
                return Ok("OK");
            }
            catch (Exception ex)
            {
                return BadRequest("Error en Creacion: " + ex.Message);
            }
        }

        [System.Web.Http.HttpPut]
        [System.Web.Mvc.Route("api/departamento/{id}")]
        public IHttpActionResult Edit(int? id, Departamento departamento)
        {

            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                db.Entry(departamento).State = EntityState.Modified;
                db.SaveChanges();
                return Ok("Dato modificado exitosamente");
            }
            catch (Exception ex)
            {
                return BadRequest("Error en modificacion: " + ex.Message);
            }
        }

        [System.Web.Http.HttpDelete]
        [System.Web.Mvc.Route("api/departamento/{id}")]
        public IHttpActionResult Delete(int id)
        {
            try
            {
                Departamento departamento = db.Departamento.Find(id);
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                db.Entry(departamento).State = EntityState.Deleted;
                db.SaveChanges();
                return Ok("Datos eliminados exitosamente");
            }
            catch (Exception ex)
            {
                return BadRequest("Error en Eliminación: " + ex.Message);
            }

        }
    }
}